<div class="grid-full">
  <?php print $content['top']; ?>    
</div>

<div class="grid-full clearfix">
  <div class="grid-3 grid-first">
    <?php print $content['left']; ?>  
  </div>
  <div class="grid-6">
    <?php print $content['middle']; ?>  
  </div>
  <div class="grid-3 grid-last">
    <?php print $content['right']; ?>  
  </div>
</div>

<div class="grid-full">
  <?php print $content['bottom']; ?>  
</div>
