<?php
/* =====================================
  template.php
* ------------------------------------- */

/**
 * Implements theme_menu_item_link()
 * yup original is from zen and we humbly say thanx :)
 */
function neuehaasgrotesk_menu_item_link($link) {
  if (empty($link['localized_options'])) {
    $link['localized_options'] = array();
  }

  //define a class for each tab 
  //TODO check out the multilang hell....
  $linkclass = mothership_id_safe($link['title']);
  
  // If an item is a LOCAL TASK, render it as a tab
  if($link['type'] & MENU_IS_LOCAL_TASK) {
    $link['title'] = '<span class="tab '. $linkclass .' ">' . check_plain($link['title']) . '</span>';
    $link['localized_options']['html'] = TRUE;
    $link['localized_options']['attributes'] = array('class' => $linkclass);
  }else{
    // its a standard menu item lets kick in a span class
    $link['title'] = '<span class="icon-'. $linkclass .' ">' . check_plain($link['title']) . '</span>';
    $link['localized_options']['html'] = TRUE;
    $link['localized_options']['attributes'] = array('class' => $linkclass);
  }
  
  return l($link['title'], $link['href'], $link['localized_options']);
}

function neuehaasgrotesk_links($links, $attributes = array('class' => 'links')) {

  global $language;
  $output = '';

  if (count($links) > 0) {
    $output = '<ul'. drupal_attributes($attributes) .'>';

    $num_links = count($links);
    $i = 1;

    foreach ($links as $key => $link) {
      $class = $key;

      // Add first, last and active classes to the list of links to help out themers.
      if ($i == 1) {
        $class .= ' first';
      }
      if ($i == $num_links) {
        $class .= ' last';
      }
      if (isset($link['href']) && ($link['href'] == $_GET['q'] || ($link['href'] == '<front>' && drupal_is_front_page()))
          && (empty($link['language']) || $link['language']->language == $language->language)) {
        $class .= ' active';
      }
      $output .= '<li'. drupal_attributes(array('class' => $class)) .'>';

      if (isset($link['href'])) {
        // Pass in $link as $options, they share the same keys.
        $output .= l($link['title'], $link['href'], $link);
      }
      else if (!empty($link['title'])) {
        // Some links are actually not links, but we wrap these in <span> for adding title and class attributes
        if (empty($link['html'])) {
          $link['title'] = check_plain($link['title']);
        }
        $span_attributes = '';
        if (isset($link['attributes'])) {
          $span_attributes = drupal_attributes($link['attributes']);
        }
        $output .= '<span'. $span_attributes .'>'. $link['title'] .'</span>';
      }

      $i++;
      $output .= "</li>\n";
    }

    $output .= '</ul>';
  }

  return $output;
}
