<?php
/**
 * @file forum-icon.tpl.php
 * Display an appropriate icon for a forum post.
 *
 * Available variables:
 * - $new_posts: Indicates whether or not the topic contains new posts.
 * - $icon: The icon to display. May be one of 'hot', 'hot-new', 'new',
 *   'default', 'closed', or 'sticky'.
 *
 * @see template_preprocess_forum_icon()
 * @see theme_forum_icon()
 *
 * mothership note: were removing the img file and adding it in as a class for a div
 * giving us the possibility to add the icons as a sprite instead of a single image
 */
?>

<?php if ($new_posts): ?>
  <a name="new">
<?php endif; ?>

<div class="icon-forum-<?php print $icon; ?><?php if ($new_posts){ print " icon-forum-new"; } ?>"></div>

<?php if ($new_posts): ?>
  </a>
<?php endif; ?>
