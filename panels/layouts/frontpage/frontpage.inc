<?php
/**
 * Implementation of HOOK_panels_layouts().
 */
function neuehaasgrotesk_frontpage_panels_layouts() {
  $items['frontpage'] = array(
    'title' => t('Frontpage'),
    'icon' => 'frontpage.png',
    'theme' => 'frontpage',
    'css' => 'frontpage.css',
    'panels' => array(
      'top' => t('top'),
      'left' => t('Left'),
      'right' => t('Right'),
      'middle' => t('Middle'),      
      'bottom' => t('bottom'),
    ),
  );

  return $items;
}