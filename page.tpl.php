<?php
/* *
 * @file
 * page.tpl.php
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language; ?>" lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>">
<!--
  Neue Haas Grotesk
-->
<head>
  <title><?php print $head_title; ?></title>
  <?php print $head; ?>
  <?php print $styles; ?>
  <?php print $scripts; ?>
</head>
<body class="<?php print $body_classes; ?>">

<?php if (!empty($admin)) print $admin; ?>

<div id="container" class="grid-full center">
  <div id="container-inner" class="grid-12 center">
		<!-- slider -->
		<div id="slider">
		  <div id="slider-inner">
		    <div id="slider-background"></div>
			  	<?php if ($slider) { ?>
			  	  <?php print $slider; ?>
			  	<?php } ?>

          <?php print mothership_userprofile($user); ?>

		  </div>
		 	<div class="slider-button"><a href="#"><?php print t('login'); ?></a> </div>
		  <div class="slider-button" id="hide_button"><a href="#"><?php print t('hide'); ?></a></div>
		</div>
		<!-- /slider -->

    <div id="header" class="clearfix">
      <div id="header-inner">

        <<?php print $site_name_element; ?> id="site-name">
          <a href="<?php print $base_path; ?>" title="<?php print t('Home'); ?>" rel="home"><?php print $site_name; ?></a>
        </<?php print $site_name_element; ?>>

        <?php if ($header) { ?>
          <div id="header-block">
            <?php print $header; ?>            
          </div>
        <?php } ?>

      </div>
    </div>

    <?php if ($breadcrumb) { ?>
      <div id="path" class="grid-12 grid-first grid-last">
        <div id="path-inner">
          <?php print $breadcrumb; ?>
        </div>
      </div>
    <?php } ?>

    <?php if ($help OR $messages) { ?>
      <div id="drupal-messages" class="">
          <?php print $help ?>
          <?php print $messages ?>
      </div>
    <?php } ?>

    <div id="page">
      <div id="page-inner" class="clearfix">

        <div id="pageheader" class="clearfix"></div>

        <div id="pagebody" class="clearfix">
          <div id="pagebody-inner" class="clearfix">

            <?php if ($left) { ?>
              <div id="sitebar-first" class="grid-3 grid-first">
                <?php print $left; ?>              
              </div>
            <?php } ?>

            <div id="content" class="grid-6">
              <div id="content-inner">
                <?php if ($tabs) { ?>
                  <div id="tabs"><?php print $tabs; ?></div>
                <?php }; ?>
                <?php if ($title AND (arg(0) != "node")) {  ?>
                  <h1><?php print $title; ?></h1>
                <?php } ?>
                <?php if ($content_top) { ?>
                  <?php print $content_top; ?>
                <?php } ?>
                <?php print $content; ?>
                <?php if ($content_bottom) { ?>
                  <?php print $content_bottom; ?>
                <?php } ?>

              </div>
            </div>

            <?php if ($right) { ?>
              <div id="sitebar-last"  class="grid-3 grid-last">
                <?php print $right; ?>
              </div>
            <?php } ?>

          </div>
        </div>

        <div id="pagefooter" class="grid-full">
          <div id="pagefooter-inner">
              <?php print $footer; ?>
          </div>
        </div>

      </div>
    </div>

    </div>

    <div id="footer"></div>

</div>
<?php print $closure; ?>
</body>
</html>