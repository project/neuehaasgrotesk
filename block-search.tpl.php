<?php
/**
 * @file
 * block-search.tpl
 * remove that header once and for all
 */

/*
ad a class="" if we have anything in the $classes var
this is so we can have a cleaner output - no reason to have an empty <div class="" id="">
 */
if ($classes) {
  $classes = ' class="' . $classes . '"';
}

if ($id_block) {
  $id_block = ' id="' . $id_block . '"';
}
?>

<div<?php print $id_block . $classes; ?>>
  <?php print $block->content; ?>
  <?php  print $edit_links; ?>
</div>